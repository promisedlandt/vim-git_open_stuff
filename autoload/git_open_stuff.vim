" {{{ Open Ticket
" Get the current git branch name, and open the ticket for that branch
function! git_open_stuff#Ticket()
  let branch_name = git_open_stuff#util#git#current_branch_name()

  call git_open_stuff#TicketByBranchName(branch_name)
endfunction

" Open the ticket for a given branch name
function! git_open_stuff#TicketByBranchName(branch_name)
  let ticket_url = git_open_stuff#jira#ticket#build_url(a:branch_name)

  call git_open_stuff#util#browser#open_url(ticket_url)
endfunction
" }}}

" {{{ Open Merge Request
" Get the current git branch name, and open the merge request for that branch
function! git_open_stuff#MergeRequest()
  let remote_domain = git_open_stuff#util#git#remote_domain()
  let gitlab_domains = get(g:, "fugitive_gitlab_domains", []) + ["gitlab.com"]
  let github_domains = get(g:, "github_enterprise_urls", []) + ["github.com"]

  if match(github_domains, remote_domain) > -1
    return git_open_stuff#GithubMergeRequest()
  elseif match(gitlab_domains, remote_domain) > -1
    return git_open_stuff#GitlabMergeRequest()
  else
    echoerr "Could not determine hoster type from " . remote_domain

    return
  endif
endfunction

function! git_open_stuff#GitlabMergeRequest() abort
  let git_head_sha = git_open_stuff#util#git#head_sha()

  call git_open_stuff#GitlabMergeRequestBySha(git_head_sha)
endfunction

function! git_open_stuff#GitlabMergeRequestBySha(git_sha) abort
  let mr_url = git_open_stuff#gitlab#merge_request#build_url(a:git_sha)

  call git_open_stuff#util#browser#open_url(mr_url)
endfunction

function! git_open_stuff#GithubMergeRequest() abort
  let branch_name = git_open_stuff#util#git#current_branch_name()

  call git_open_stuff#GithubMergeRequestByBranchName(branch_name)
endfunction

" Open the merge request for a given branch name
function! git_open_stuff#GithubMergeRequestByBranchName(branch_name)
  let merge_request_url = git_open_stuff#github#merge_request#build_url(a:branch_name)

  call git_open_stuff#util#browser#open_url(merge_request_url)
endfunction
" }}}
