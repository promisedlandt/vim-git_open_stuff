function! git_open_stuff#util#git#current_branch_name()
  return trim(system('git symbolic-ref --short -q HEAD'))
endfunction

function! git_open_stuff#util#git#head_sha()
  return trim(system('git log --pretty=format:"%H" -n 1'))
endfunction

function! git_open_stuff#util#git#remote_domain()
  return trim(system('git remote get-url origin | grep "(?<=\@)[^:]*" --only-matching --perl-regexp'))
endfunction
