function! git_open_stuff#github#merge_request#build_url(branch_name)
  return string(trim(system('hub pr list --head="' . a:branch_name . '" --format="%U" --state="all"')))
endfunction
