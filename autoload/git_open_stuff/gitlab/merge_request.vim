function! git_open_stuff#gitlab#merge_request#build_url(git_sha) abort
  " domain:username/repo
  let remote_url = trim(system('git remote get-url origin | grep "(?<=git\@).*:.*(?=\.git)" --only-matching --perl-regexp'))
  let repo_parts = split(remote_url, ":")
  let repo_domain = repo_parts[0]
  let username_and_project = repo_parts[1]

  " need to url-encode the slash: promisedlandt/example_project -> promisedlandt%2Fexample_project
  let url_escaped_username_and_project = substitute(username_and_project, "/", "%2F", "")
  let url = "https://".repo_domain."/api/v4/projects/".url_escaped_username_and_project."/repository/commits/".a:git_sha."/merge_requests"
  let curl_string = 'curl --silent --header "Private-Token: '.$GITLAB_API_KEY.'" "'.url.'"'
  let gitlab_response = trim(system(curl_string))
  let mr_id = json_decode(gitlab_response)[0].iid

  return string("https://".repo_domain."/".username_and_project."/merge_requests/".mr_id)
endfunction
