function! git_open_stuff#jira#ticket#build_url(branch_name)
  return string('https://' . $TICKET_TRACKER_BASE_URL . '/browse/'. a:branch_name)
endfunction
