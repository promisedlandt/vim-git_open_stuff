# git_open_stuff

Vim / NeoVim plugin to open JIRA tickets and existing GitLab / GitHub pull requests for the git branch you're currently working on.

As this was just extracted from my config, there is little configuration, and an almost empty README. Please see the documentation under `doc/git_open_stuff.txt` to learn about the prerequisites for using this plugin.
