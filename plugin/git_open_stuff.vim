if exists('g:loaded_git_open_stuff')
  finish
endif
let g:loaded_git_open_stuff = 1

command! -nargs=0 GitOpenStuffTicket call git_open_stuff#Ticket()
command! -nargs=0 GitOpenStuffMergeRequest call git_open_stuff#MergeRequest()
command! -nargs=0 GitOpenStuffGitlabMergeRequest call git_open_stuff#GitlabMergeRequest()
command! -nargs=0 GitOpenStuffGithubMergeRequest call git_open_stuff#GithubMergeRequest()

nnoremap <silent> <Plug>(GitOpenStuffTicket) :GitOpenStuffTicket<cr>
nnoremap <silent> <Plug>(GitOpenStuffMergeRequest) :GitOpenStuffMergeRequest<cr>
